package com.yip.labs.product.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.yip.labs.common.dto.ReviewDto;
import com.yip.labs.product.persistence.ReviewRepository;
import com.yip.labs.product.entity.Review;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
@Service
public class ReviewService {
	final ReviewRepository reviewRepository;

	public List<ReviewDto> findAll() {
		log.debug("Request to get all Reviews");
		return this.reviewRepository.findAll().stream().map(ReviewService::mapToDto).collect(Collectors.toList());
	}

	@Transactional(readOnly = true)
	@HystrixCommand(fallbackMethod="getDefaultReviewId")
	public ReviewDto findById(Long id) {
		log.debug("Request to get Review : {}", id);
//		return this.reviewRepository.findById(id).map(ReviewService::mapToDto).orElse(null);
		return this.reviewRepository.findById(id).map(ReviewService::mapToDto).get();
	}
	
	public ReviewDto getDefaultReviewId(Long id) {
		log.debug("getDefaultReviewId", id);
		return new ReviewDto(id, "UNKNOW", "NONE", 0l);
	}

	public ReviewDto create(ReviewDto reviewDto) {
		log.debug("Request to create Review : {}", reviewDto);
		return mapToDto(this.reviewRepository
				.save(new Review(reviewDto.getTitle(), reviewDto.getDescription(), reviewDto.getRating())));
	}

	public void delete(Long id) {
		log.debug("Request to delete Review : {}", id);
		this.reviewRepository.deleteById(id);
	}

	public static ReviewDto mapToDto(Review review) {
		if (review != null) {
			return new ReviewDto(review.getId(), review.getTitle(), review.getDescription(), review.getRating());
		}
		return null;
	}
}