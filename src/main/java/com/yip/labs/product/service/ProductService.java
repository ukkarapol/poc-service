package com.yip.labs.product.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.yip.labs.common.dto.ProductDto;
import com.yip.labs.product.entity.Product;
import com.yip.labs.product.entity.ProductStatus;
import com.yip.labs.product.persistence.CategoryRepository;
import com.yip.labs.product.persistence.ProductRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
@Service
@Transactional
public class ProductService {
	private final ProductRepository productRepository;
	private final CategoryRepository categoryRepository;

	public List<ProductDto> findAll() {
		log.debug("Request to get all Products");
		return this.productRepository.findAll().stream().map(ProductService::mapToDto).collect(Collectors.toList());
	}
	
	@Transactional(readOnly = true)
	@HystrixCommand(fallbackMethod="getDefaultProductId")
	public ProductDto findById(Long id) {
		log.debug("Request to get Product : {}", id);
//		return this.productRepository.findById(id).map(ProductService::mapToDto).orElse(null);
		return this.productRepository.findById(id).map(ProductService::mapToDto).get();
//		Product product = this.productRepository.getOne(id);
//		log.debug("1 :: findById product ==> ", product);
//		log.debug("2 :: findById product ==> ", product.getId());
//		return mapToDto(product);
	}
	
	
	ProductDto getDefaultProductId(Long id) {
		log.debug("getDefaultProductId", id);
		ProductDto productDto = new ProductDto();
		productDto.setId(id);
		productDto.setName("UNKNOW");
		productDto.setDescription("NONE");
		return productDto;
	}

	public ProductDto create(ProductDto productDto) {
		log.debug("Request to create Product : {}", productDto);
		return mapToDto(
				this.productRepository.save(
					new Product(
							productDto.getName(), 
							productDto.getDescription(),
							productDto.getPrice(), 
							productDto.getQuantity(), 
							ProductStatus.valueOf(productDto.getStatus()),
							productDto.getSalesCounter()
							, null
							, this.categoryRepository.
								findById(productDto.getCategory().getId()).orElse(null)
							)));
	}

	public void delete(Long id) {
		log.debug("Request to delete Product : {}", id);
		this.productRepository.deleteById(id);
	}

	public static ProductDto mapToDto(Product product) {
		log.debug("::ProductDto mapToDto product  ::"+product);
		if (product != null) {
			return new ProductDto(
					product.getId(), 
					product.getName(), 
					product.getDescription(), 
					product.getPrice(),
					product.getQuantity(), 
					product.getStatus().name(), 
					product.getSalesCounter()
					,product.getReviews().stream().
					map(ReviewService::mapToDto).collect(Collectors.toSet())
//					,null
					,CategoryService.mapToDto(product.getCategory())
//					,null
					);
		}
		return null;
	}
}
