package com.yip.labs.product.entity;

public enum ProductStatus {
	AVAILABLE, DISCONTINUED
}
