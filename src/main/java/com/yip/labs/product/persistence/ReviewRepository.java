package com.yip.labs.product.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yip.labs.product.entity.Review;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {
}
