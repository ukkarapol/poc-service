#!groovy
// Jenkinsfile for Poc-Service
podTemplate(
  label: "jenkins-pod",
  cloud: "openshift",
  inheritFrom: "maven",
  containers: [
    containerTemplate(
      name: "jnlp",
      image: "docker-registry.default.svc:5000/${GUID}-tools/jenkins-agent-appdev",
      resourceRequestMemory: "1Gi",
      resourceLimitMemory: "2Gi"
    )
  ]
) {
  node('jenkins-pod') {
  
  	def mvnCmd = "mvn -s artifact_setting.xml clean package -DskipTests=true" 

    def version = getVersionFromPom("pom.xml")

  	// TBD Set the tag for the development image: version + build number
  	def devTag  = "${version}-${BUILD_NUMBER}"
  	// Set the tag for the production image: version
  	def prodTag = "${version}-prod" 

	    stage('pull code') {
	      git url: "${REPO}"
	    }
      
	  stage('Build') {
	    echo "Building version ${devTag}"
	    sh "${mvnCmd} clean package -DskipTests"
	    // TBD: Execute Maven Build
	  }

      // TBD: The next two stages should run in parallel

      // Using Maven run the unit tests
      stage('Unit Tests') {
        echo "Running Unit Tests"
        sh "${mvnCmd} test"
        // TBD: Execute Unit Tests
      }

      stage('Scan Code/Lib') {
        echo "Running Code Analysis"
        sh "${mvnCmd} sonar:sonar -Dsonar.host.url=http://sonarqube-${GUID}-tools.cloudapps.inet.local -Dsonar.projectName=${JOB_BASE_NAME}-${devTag}"
        // TBD: Execute Sonarqube Tests
      }
      
      stage('Pack Container') {
        echo "Pack Container"
        sh "${mvnCmd} deploy -DskipTests=true -DaltDeploymentRepository=central::default::http://172.26.1.23:8081/artifactory/libs-release/"
        // TBD: Publish to Nexus
      }
      
      stage('Scan Container') {
        echo "Scan Container"
        sh "${mvnCmd} sonar:sonar -Dsonar.host.url=http://sonarqube-${GUID}-tools.cloudapps.inet.local -Dsonar.projectName=${JOB_BASE_NAME}-${devTag}"
        // TBD: Execute Sonarqube Tests
      }
      
      stage('Push to Registry') {
        echo "Push to Registry"
        // TBD: Publish to Nexus
      }
      
      stage('Deploy to STG') {
        echo "Push to Registry"
        // TBD: Publish to Nexus
      }
      
      stage('Automate Testing') {
        echo "Automate Testing"
        // TBD: Publish to Nexus
      }
      
      stage('Release Approval') {
        echo "Release Approval"
        // TBD: Publish to Nexus
      }
      
      stage('Deploy') {
        echo "Deploy"
        // TBD: Publish to Nexus
      }
      
  }
}

// Convenience Functions to read variables from the pom.xml
// Do not change anything below this line.
def getVersionFromPom(pom) {
  def matcher = readFile(pom) =~ '<version>(.+)</version>'
  matcher ? matcher[0][1] : null
}
def getGroupIdFromPom(pom) {
  def matcher = readFile(pom) =~ '<groupId>(.+)</groupId>'
  matcher ? matcher[0][1] : null
}
def getArtifactIdFromPom(pom) {
  def matcher = readFile(pom) =~ '<artifactId>(.+)</artifactId>'
  matcher ? matcher[0][1] : null
}

